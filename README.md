# vatLARA Configurations
Repository of VATSIM's vACC's configuration files used by vLARA to synchronously activate areas. 

## Contributing
If your vACC is not yet participating, or you'd like to help help out, create a new issue with your VATSIM id, role and Discord tag. 

Use the [LARA-configurator](https://github.com/MatisseBE/LARA-Configurator) to get started.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
In development.
